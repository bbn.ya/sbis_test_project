
config={
    'content_block': ['div', 'article'],
    'content_block_attr_name': 'class',
    'content_block_attr_value': ['main_article', ".*content.*", 'post'],
    'tags_remove': ['script', 'style', 'meta', 'form', 'button','time'],
    'style_remove': ['.*comment.*', '.*tags.*','.*banner.*','.*social.*','.*polling.*','.*auth.*'],
    'text_isset_remove': ['.*Читайте также.*', '.*.Интересное в сети*','.*Тег.*'],
    'block_for_add_br': ['p', 'h1', 'h2', 'h3', 'h4']
}