from urllib.request import urlopen, Request
from urllib.error import HTTPError
from urllib.error import URLError
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import re
import textwrap
import os
import sys

from config import config


def is_valid_url(url):
    regex = re.compile(
        r'^https?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return url is not None and regex.search(url)


def get_html(url):
    try:
        req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        html = urlopen(req)
    except HTTPError as e:
        print(e)
        return None
    except URLError as e:
        print(e)
        return None
    except BaseException as e:
        print(e)
        return None
    except UnicodeError as e:
        print(e)
        return None

    return html


def generate_text(html,url=''):
    try:
        array=[]
        bsObj = BeautifulSoup(html.read(), features="lxml")
        content = bsObj.find('h1')
        if(isinstance(content, (type(None), bytes))):
            content = bsObj.find(re.compile('|'.join(config.get('content_block', '.*content.*'))),
                                 {config.get('content_block_attr_name', 'class'),
                                  re.compile('|'.join(config.get('content_block_attr_value', '.*content.*')))})
        else:
            content = content.find_parent(re.compile('|'.join(config.get('content_block', '.*content.*'))),
                                          {config.get('content_block_attr_name', 'class'),
                                           re.compile('|'.join(config.get('content_block_attr_value', '.*content.*')))})

        if (isinstance(content, (type(None), bytes))):
            print('Content not found')
            return
        else:
            for delete_tag in content(config.get('tags_remove', ['script'])):  # remove all javascript and stylesheet code
                delete_tag.extract()
            for delete_class in content.find_all(['div', 'span', 'ul', 'dt', 'dl', 'p'], {'class': re.compile('|'.join(config.get('style_remove', ['.*comment.*'])))}):
                delete_class.decompose()
            for delete_text_isset_block in content.find_all(['div'], text=re.compile('|'.join(config.get('text_isset_remove')))):
                delete_text_isset_block.find_parent().decompose()
            for div in content.find_all("div"):
                array.append(div)
            for tag in content.find_all('a'):
                if(tag.string != None):
                    if(is_valid_url(tag['href'])):
                        tag.replace_with("%s [%s]" % (tag.string, tag['href']))
                    else:
                        tag.replace_with("%s [%s%s]" % (tag.string, url, tag['href']))
                else:
                    tag.decompose()
            for tag in content.find_all(config.get('block_for_add_br', ['p', 'h1'])):
                if(tag.string != None):
                    tag.replace_with("%s [add_br]" % (tag.string))
                else:
                    tag.decompose()
            for tag in content.find_all('li'):
                if(tag.string != None):
                    tag.replace_with("- %s" % (tag.string))
                else:
                    tag.decompose()
        text = content.text.split('\n')
        text = ["\n".join(textwrap.wrap(x, 80)) for x in text if x != '']
        text = '\n'.join(text)
        text = text.replace('[add_br]', '\n')
    except AttributeError as e:
        print('Parsing error')
        print(e)
        return None
    return text

def readability(url):
    parsed_uri = urlparse(url)
    parsed_domain = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
    html = get_html(url)
    content = generate_text(html,parsed_domain)
    try:
        tmp_ulr_array = list(filter(None, url.split('/')))
        content_path = 'content'+os.sep+os.sep.join(tmp_ulr_array[1:-1])
        if not os.path.exists(content_path):
            os.makedirs(content_path)
        file_name=tmp_ulr_array[-1]+'.txt'
        f = open('%s/%s' % (content_path, file_name), 'w')
        f.write(content)
        f.close()
        print("Add file: %s/%s" % (content_path, file_name) )
    except BaseException as e:
        print(e)





if __name__ == "__main__":
    if len (sys.argv) > 1:
        url=sys.argv[1]
        readability(url)

    else:
        print("please, run script with URL")





